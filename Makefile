#
# Makefile 
# state machine
#

CC=dmd
CFLAGS=-H -gc

FILES=machine/entities.d machine/elsa.d machine/miner.d main.d
CLEAN=main

all: main

main: ${FILES}
	${CC} $^ -ofmain ${CFLAGS}

clean: ${CLEAN}
	rm $^
