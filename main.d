import std.random;
import core.thread;
import m = machine.miner;
import el = machine.elsa;
import e = machine.entities;

template Entity(T) {
  alias Enter = void function(T *that);
  alias Execute = int function(T *that);
}

Entity!(m.Miner).Enter[] enter = [&m.enter_mine_dig_enter, &m.visit_bank_deposit_enter, &m.go_home_sleep_enter, &m.quench_thirst_enter];
Entity!(m.Miner).Execute[] execute = [&m.enter_mine_dig_execute, &m.visit_bank_deposit_execute, &m.go_home_sleep_execute, &m.quench_thirst_execute];
Entity!(m.Miner).Enter[] exit = [&m.enter_mine_dig_exit, &m.visit_bank_deposit_exit, &m.go_home_sleep_exit, &m.quench_thirst_exit];
Entity!(el.Elsa).Enter[] enter_elsa = [&el.do_house_work_enter, &el.visit_bathroom_enter];
Entity!(el.Elsa).Execute[] execute_elsa = [&el.do_house_work_execute, &el.visit_bathroom_execute];
Entity!(el.Elsa).Enter[] exit_elsa = [&el.do_house_work_exit, &el.visit_bathroom_exit];

void main() {
  m.Miner *miner = m.miner_create(e.Entities.ent_Miner_Bob, e.EntitiesStr[e.Entities.ent_Miner_Bob]);
  int state = e.MinerStates.go_home_sleep, state_prev;
  el.Elsa *elsa = el.elsa_create(e.Entities.ent_Elsa, e.EntitiesStr[e.Entities.ent_Elsa]);
  int state_elsa = e.ElsaStates.do_house_work, state_prev_elsa;
  foreach(i; 0..20) {
    state_prev = state;
    enter[state](miner);
    state = execute[state](miner);
    if(state_prev - state) {
      exit[state_prev](miner);
    }
    e.message_receive(elsa, uniform(0, 2));
    state_prev_elsa = state_elsa;
    enter_elsa[state_elsa](elsa);
    state_elsa = execute_elsa[state_elsa](elsa);
    if(state_prev_elsa - state_elsa) {
      exit_elsa[state_prev_elsa](elsa);
    }
    Thread.sleep(dur!("msecs")(800));
  }
  m.miner_destroy(&miner);
  el.elsa_destroy(&elsa);
}
