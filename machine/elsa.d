module machine.elsa;

import std.stdio;
import std.random;
import core.stdc.stdlib;
import machine.entities;

struct Elsa {
  int id;
  string name;
  int[] messages;
}

Elsa *elsa_create(int id, string name) {
  Elsa *that = cast(Elsa*)malloc(Elsa.sizeof);
  that.id = id;
  that.name = name;
  return that;
}
void elsa_destroy(Elsa **that) {
  if(*that == null) {
    return;
  }
  free(*that);
  *that = null;
}
void do_house_work_enter(Elsa *that) {
}
int do_house_work_execute(Elsa *that) {
  int message = message_get(that);
  switch(message) {
    case 0:
      writef("Moppin' the floor. " ~ that.name ~ "\n"); 
      break;
    case 1: 
      writef("Cooking the dinner. " ~ that.name ~ "\n");
      return ElsaStates.visit_bathroom;
    default: break;
  }
  return ElsaStates.do_house_work;
}
void do_house_work_exit(Elsa *that) {
}
void visit_bathroom_enter(Elsa *that) {
}
int visit_bathroom_execute(Elsa *that) {
  writef("Ahhhhhhh! Sweet releif! " ~ that.name ~ "\n");
  return ElsaStates.do_house_work;
}
void visit_bathroom_exit(Elsa *that) {
  writef("Leavin' the john. " ~ that.name ~ "\n");
}
